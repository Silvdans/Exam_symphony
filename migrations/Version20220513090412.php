<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220513090412 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE company (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, nationality VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stop (id INT AUTO_INCREMENT NOT NULL, city VARCHAR(255) NOT NULL, date DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tour (id INT AUTO_INCREMENT NOT NULL, company_id INT NOT NULL, main_event VARCHAR(255) NOT NULL, capacity INT NOT NULL, price DOUBLE PRECISION NOT NULL, start_date DATE NOT NULL, stop_date DATE NOT NULL, INDEX IDX_6AD1F969979B1AD6 (company_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tour_stop (tour_id INT NOT NULL, stop_id INT NOT NULL, INDEX IDX_9866F69615ED8D43 (tour_id), INDEX IDX_9866F6963902063D (stop_id), PRIMARY KEY(tour_id, stop_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE tour ADD CONSTRAINT FK_6AD1F969979B1AD6 FOREIGN KEY (company_id) REFERENCES company (id)');
        $this->addSql('ALTER TABLE tour_stop ADD CONSTRAINT FK_9866F69615ED8D43 FOREIGN KEY (tour_id) REFERENCES tour (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tour_stop ADD CONSTRAINT FK_9866F6963902063D FOREIGN KEY (stop_id) REFERENCES stop (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tour DROP FOREIGN KEY FK_6AD1F969979B1AD6');
        $this->addSql('ALTER TABLE tour_stop DROP FOREIGN KEY FK_9866F6963902063D');
        $this->addSql('ALTER TABLE tour_stop DROP FOREIGN KEY FK_9866F69615ED8D43');
        $this->addSql('DROP TABLE company');
        $this->addSql('DROP TABLE stop');
        $this->addSql('DROP TABLE tour');
        $this->addSql('DROP TABLE tour_stop');
    }
}
