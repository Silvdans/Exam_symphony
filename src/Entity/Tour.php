<?php

namespace App\Entity;

use App\Repository\TourRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TourRepository::class)]
class Tour
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $mainEvent;

    #[ORM\Column(type: 'integer')]
    private $capacity;

    #[ORM\Column(type: 'float')]
    private $price;

    #[ORM\Column(type: 'date')]
    private $startDate;

    #[ORM\Column(type: 'date')]
    private $stopDate;

    #[ORM\ManyToMany(targetEntity: Stop::class, inversedBy: 'tour')]
    private $Stop;

    #[ORM\ManyToOne(targetEntity: Company::class, inversedBy: 'tours')]
    #[ORM\JoinColumn(nullable: false)]
    private $company;

    public function __construct()
    {
        $this->Stop = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMainEvent(): ?string
    {
        return $this->mainEvent;
    }

    public function setMainEvent(string $mainEvent): self
    {
        $this->mainEvent = $mainEvent;

        return $this;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(int $capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }
    public function getStringStartDate(){
        return $this->getStartDate()->format('Y-m-d');
    }
    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getStringStopDate(){
        return $this->getStopDate()->format('Y-m-d');
    }
    public function getStopDate(): ?\DateTimeInterface
    {
        return $this->stopDate;
    }

    public function setStopDate(\DateTimeInterface $stopDate): self
    {
        $this->stopDate = $stopDate;

        return $this;
    }

    /**
     * @return Collection<int, Stop>
     */
    public function getStop(): Collection
    {
        return $this->Stop;
    }

    public function addStop(Stop $stop): self
    {
        if (!$this->Stop->contains($stop)) {
            $this->Stop[] = $stop;
        }

        return $this;
    }

    public function removeStop(Stop $stop): self
    {
        $this->Stop->removeElement($stop);

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }
}
