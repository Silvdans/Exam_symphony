<?php

namespace App\Entity;

use App\Repository\StopRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: StopRepository::class)]
class Stop
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $city;

    #[ORM\Column(type: 'date')]
    private $date;

    #[ORM\ManyToMany(targetEntity: Tour::class, mappedBy: 'Stop')]
    private $tour;

    public function __construct()
    {
        $this->tour = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Collection<int, Tour>
     */
    public function getTour(): Collection
    {
        return $this->tour;
    }

    public function addTour(Tour $tour): self
    {
        if (!$this->tour->contains($tour)) {
            $this->tour[] = $tour;
            $tour->addStop($this);
        }

        return $this;
    }

    public function removeTour(Tour $tour): self
    {
        if ($this->tour->removeElement($tour)) {
            $tour->removeStop($this);
        }

        return $this;
    }
    public function getCompanies() : array
    {
        $array = [];
        foreach ($this->getTour() as $tour){
            array_push($array, $tour->getCompany());
        }
        return $array;
    }
}
