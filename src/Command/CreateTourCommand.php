<?php

namespace App\Command;

use App\Entity\Tour;
use App\Repository\CompanyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'CreateTour',
    description: 'Add a short description for your command',
)]
class CreateTourCommand extends Command
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager, CompanyRepository $companyRepository) {
        parent::__construct();
        $this->entityManager = $entityManager;
        $this->companyRepository = $companyRepository;
    }
    protected function configure(): void
    {
        $this
            ->addOption('mainEvent', null, InputOption::VALUE_REQUIRED, 'main event of the tour')
            ->addOption('capacity', null, InputOption::VALUE_REQUIRED, 'capacity of the tour')
            ->addOption('price', null, InputOption::VALUE_REQUIRED, 'price of the tour')
            ->addOption('company_id', null, InputOption::VALUE_REQUIRED, 'company of the tour')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $companyRepository = $this->companyRepository;

        $tourMainEvent = $input->getOption('mainEvent');
        $tourCapacity = $input->getOption('capacity');
        $tourPrice = $input->getOption('price');
        $tourCompanyId = $input->getOption('company_id');
        $tourCompany = $companyRepository->find($tourCompanyId);

        $tour= new Tour();
        $tour->setMainEvent($tourMainEvent);
        $tour->setCapacity($tourCapacity);
        $tour->setPrice($tourPrice);
        $datestart = new \DateTime();
        $datestart->setDate(2022, 06, 01);
        $datestop = new \DateTime();
        $datestop->setDate(2022, 07, 01);
        $tour->setStartDate($datestart);
        $tour->setStopDate($datestop);
        $tour->setCompany($tourCompany);

        $this->entityManager->persist($tour);
        $this->entityManager->flush();

        $io->success('Le tour numéro'.$tour->getId().'est crée.');

        return Command::SUCCESS;
    }
}
