<?php

namespace App\Command;

use App\Repository\CompanyRepository;
use \Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'ListTourCompany',
    description: 'Add a short description for your command',
)]
class ListTourCompanyCommand extends Command
{
    private $companyRepository;

    public function __construct(CompanyRepository $companyRepository) {
        parent::__construct();
        $this->companyRepository = $companyRepository;
    }
    protected function configure(): void
    {
        $this
            ->addOption('companyName', null, InputOption::VALUE_REQUIRED, 'Name of the company')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $companyRepository = $this->companyRepository;

        $company_name = $input->getOption('companyName');
        $company = $companyRepository->findOneBy(['name' => $company_name]);
        $tours = $company->getTours();

        $table = new Table($output);
        $table->setHeaders(['id','mainEvenv','capacity','price','startDate','stopDate']);
        foreach ($tours as $tour){
            $table->addRow([$tour->getId(),$tour->getMainEvent(),$tour->getCapacity(),$tour->getPrice(),$tour->getStringStartDate(),$tour->getStringStopDate()]);
        }

        $table->render();

        return Command::SUCCESS;
    }
}
