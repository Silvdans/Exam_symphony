<?php

namespace App\Command;

use App\Entity\Company;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'CreateCompany',
    description: 'Add a short description for your command',
)]
class CreateCompanyCommand extends Command
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager) {
        parent::__construct();
        $this->entityManager = $entityManager;
    }

    protected function configure(): void
    {
        $this
            ->addOption('name', null, InputOption::VALUE_REQUIRED, 'Name of the company')
            ->addOption('city', null, InputOption::VALUE_REQUIRED, 'CIty of the company')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $companyName = $input->getOption('name');
        $companyCity = $input->getOption('city');

        $company = new Company();
        $company->setName($companyName);
        $company->setNationality($companyCity);

        $this->entityManager->persist($company);
        $this->entityManager->flush();

        $io->success('La société '.$companyName.' à été crée.');

        return Command::SUCCESS;
    }
}
