<?php

namespace App\Command;

use App\Repository\TourRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'EditTourName',
    description: 'Add a short description for your command',
)]
class EditTourNameCommand extends Command
{
    public function __construct(TourRepository $tourRepository, EntityManagerInterface $entityManager) {
        parent::__construct();
        $this->tourRepository = $tourRepository;
        $this->entityManager = $entityManager;
    }
    protected function configure(): void
    {
        $this
            ->addOption('tourId', null, InputOption::VALUE_REQUIRED, 'Current tour mainEvent')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $tourRepository = $this->tourRepository;

        $idTour = $input->getOption('tourId');
        $tour = $tourRepository->find($idTour);

        $newMainEvent = $io->ask('Nouveau mainEvent : ', $tour->getMainEvent());
        $tour->setMainEvent($newMainEvent);
        $this->entityManager->flush();

        return Command::SUCCESS;
    }
}
